import React from 'react'
import Greeting from '../components/Greeting'
import Lampu from '../components/Lampu'

class Home extends React.Component{
    render(){
        return(
            <div className="Container">
                <Greeting isLogin={true}/>
                <Lampu/>
            </div>
        )
    }
}

export default Home