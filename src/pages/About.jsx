import React from 'react'
import Warna from '../components/Warna'
import Table from '../components/Table'

class About extends React.Component{
    render(){
        return(
            <div className="Container">
                <Warna/>
                <Table/>
            </div>
        )
    }
}

export default About