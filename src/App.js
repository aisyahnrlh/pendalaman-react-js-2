import React from 'react';
import logo from './logo.svg';
import './App.css';
import NavBar from './components/NavBar';
import Warna from './components/Warna';
import Lampu from './components/Lampu';
import Greetings from './components/Greeting';
import Timer from './components/Timer';
import Table from './components/Table';

function App() {
  return (
    <div>
    <NavBar/>
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
          <Warna/>
          <br/>
          <Lampu/>
          <br/>
          <Greetings isLogin={true}/>
          <br/>

        <button className="btn btn-danger">Tombol Bootstrap</button>
        
        <Table/>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
    </div>
  );
}

export default App;
