import React from 'react'
import {BrowserRouter, Route} from 'react-router-dom'
import NavBar from './components/NavBar'
import Table from './components/Table'
import About from './pages/About'
import Home from './pages/Home'

function Router(){
    return (
        <BrowserRouter>
            <NavBar/>
            <Route exact path="/" component={Home}/>
            <Route exact path="/about" component={About}/>
            <Route exact path="/table" component={Table}/>
        </BrowserRouter>
    )
}

export default Router