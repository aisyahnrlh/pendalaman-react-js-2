import React from 'react'

function GiveGreet(){
    return(
        <h1>Selamat Datang</h1>
    )
}

function Regret(){
    return(
        <h1>Maaf Anda belum login</h1>
    )
}

function Greetings(props){
    const isLogin = props.isLogin
    if(!isLogin){
        return <Regret/>
    }
    return <GiveGreet/>
}

export default Greetings