import React from 'react'

class Timer extends React.Component{
    constructor (props){
        super(props)
        this.state = { hours: 1, minutes: 5, seconds: 20 }
        this.countDown = this.countDown.bind(this)
    }

    countDown(){
        let seconds = this.state.seconds - 1
        this.setState({
            seconds: seconds
        });
    }

    render() {
        return(
            <div>
                <button onClick={this.countDown}>Start</button>
                h: {this.state.hours} m: {this.state.minutes} s: {this.state.seconds}
            </div>
        )
    }
}

export default Timer