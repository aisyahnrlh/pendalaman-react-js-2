import React from 'react'
import Column from './Column'

function Table(){
    return(
        <table className="table table-dark">
            <tr>
                <Column nama="Gottfried" kelas="React"/>
            </tr>
            <tr>
                <Column nama="Made" kelas="Vue"/>
            </tr>
        </table>
    )
}

export default Table