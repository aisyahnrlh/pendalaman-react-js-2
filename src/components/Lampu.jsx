import React from 'react'

class Lampu extends React.Component{
    constructor(props){
        super(props)
        this.state = {isLampOn: true}
        this.saklarKlik = this.saklarKlik.bind(this)
    }

    saklarKlik(){
        this.setState(state=>({
            isLampOn: !state.isLampOn
        }))
    }

    componentDidUpdate(){
        document.getElementById('lampuku').innerHTML = "State Lampu Berubah : "+this.state.isLampOn
    }

    render(){
        return(
            <div>
                <h1>Lampu</h1>
                <button className={this.state.isLampOn ? 'btn btn-success': 'btn btn-danger'} onClick={this.saklarKlik}>
                    {this.state.isLampOn ? 'Hidup': 'Mati'}
                </button>
                <p id="lampuku"></p>
            </div>
        )
    }
}

export default Lampu